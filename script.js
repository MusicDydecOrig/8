"use strict";

const numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');

const personalMovieDB = {
    count : numberOfFilms,
    movies: {},
    actors: '',
    genres: [],
    privat: false
};

for (let i = 0; i < 2; i++) {
  const vop1 = prompt("Один из последних просмотренных фильмов?", ""),
        vop2 = prompt("На сколько оцените его?", "");

    if (vop1 != null && vop2 != null && vop1 != '' && vop2 != '' && vop1.length < 50) {
        personalMovieDB.movies[vop1] = vop2;
    } else {
        alert('Ответы так себе');
        i--;
    }

}
/*
//let i = 0;
//////////////////////////////////////////////////////////
while (i < 2) {
    const vop1 = prompt("Один из последних просмотренных фильмов?", ""),
          vop2 = prompt("На сколько оцените его?", "");

    if (vop1 != null && vop2 != null && vop1 != '' && vop2 != '' && vop1.length <= 50) {
        personalMovieDB.movies[vop1] = vop2;
        i++;
    } else {
        alert('Ответы так себе');
    }
 }
//////////////////////////////////////////////////////////
let i = 0;
do {
  const vop1 = prompt("Один из последних просмотренных фильмов?", ""),
        vop2 = prompt("На сколько оцените его?", "");
        if (vop1 != null && vop2 != null && vop1 != '' && vop2 != '' && vop1.length <= 50) {
          personalMovieDB.movies[vop1] = vop2;
          i++;
      } else {
          alert('Ответы так себе');
      }
} while (i < 2);
*/

if (personalMovieDB.count < 10) {
  alert('Просмотрено довольно мало фильмов');
} else if (personalMovieDB.count >= 10 && personalMovieDB.count <= 30) {
  alert('Вы классический зритель');
} else if (personalMovieDB.count == 30) {
  alert('Вы киноман.');
} else {
  alert('Произошла ошибка');
}

console.log(personalMovieDB);